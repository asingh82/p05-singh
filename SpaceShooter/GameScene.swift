//
//  GameScene.swift
//  SpaceShooter
//
//  Created by Anshima on 29/03/17.
//  Copyright © 2017 AnshimaSingh. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var stars:SKEmitterNode!
    var player:SKSpriteNode!
    var gameplayTimer:Timer!
    var obstacleArray = ["asteroid", "Meteor", "planet.png"]
    let obstacleCategory:UInt32 = 0x1 << 1
    let torpedo:UInt32 = 0x1 << 0
    
    let right = SKAction.moveBy(x: 64, y: 0, duration: 0.4)
    let left = SKAction.moveBy(x: -64, y: 0, duration: 0.4)
    
    func swipedRight(sender:UISwipeGestureRecognizer){
        player.run(right)
    }
    func swipedLeft(sender:UISwipeGestureRecognizer){
        player.run(left)
    }
    
    override func didMove(to view: SKView) {
        
        stars = SKEmitterNode(fileNamed: "Stars")
        stars.position = CGPoint(x: 0, y:1472)
        stars.advanceSimulationTime(10)
        self.addChild(stars)
        stars.zPosition = -1
        //player.size.width =  player.size.width / 10
        //player.size.height = player.size.height / 10
        
        let swipeRight:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameScene.swipedRight))
        swipeRight.direction = .right
        view.addGestureRecognizer(swipeRight)
        
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(GameScene.swipedLeft))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
        
        
        player = SKSpriteNode(imageNamed: "Spaceship")
        player.setScale(0.3)
        
        player.position = CGPoint(x: self.frame.size.width / 100, y: -250)
        
        gameplayTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(addAlien), userInfo: nil, repeats: true)
        self.addChild(player)
        
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    
    func addAlien () {
        obstacleArray = GKRandomSource.sharedRandom().arrayByShufflingObjects(in: obstacleArray) as! [String]
        
        let obstacle = SKSpriteNode(imageNamed: obstacleArray[0])
        
        let randomAlienPosition = GKRandomDistribution(lowestValue: 0, highestValue: 414)
        let position = CGFloat(randomAlienPosition.nextInt())
        
        obstacle.position = CGPoint(x: position, y: self.frame.size.height + obstacle.size.height)
        obstacle.setScale(0.2)
        obstacle.physicsBody = SKPhysicsBody(rectangleOf: obstacle.size)
        obstacle.physicsBody?.isDynamic = true
        
        obstacle.physicsBody?.categoryBitMask = obstacleCategory
        obstacle.physicsBody?.contactTestBitMask = torpedo
        obstacle.physicsBody?.collisionBitMask = 0
        
        self.addChild(obstacle)
        
        let animationDuration:TimeInterval = 6
        
        var motion = [SKAction]()
        //motion.append(SKAction.moveTo(x: obstacle.position.x, duration: 0.5));
        motion.append(SKAction.move(to: CGPoint(x: position, y: -obstacle.size.height), duration: animationDuration))
        motion.append(SKAction.removeFromParent())
        
        obstacle.run(SKAction.sequence(motion))
        
        
    }
    
}



